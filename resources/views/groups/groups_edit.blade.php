@extends('layouts.boilerplate')

@section('content')


<form role="form" class="users_edit" method="POST">

  <?php echo Form::token(); ?>

  <div class="container">

    <h2><?php echo $title ;?></h2>

    <div class="row">
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('name', 'Name', array('class' => 'control-label')); ?>
        <?php echo Form::text('name', Input::old('name'), array('class' => 'form-control')) ?>
      </div>

    <div class="row">
      <div class="form-group col-xs-12 col-sm-5 col-md-5">
        <?php echo Form::label('slogan', 'Slogan', array('class' => 'control-label')); ?>
        <?php echo Form::text('slogan', Input::old('slogan'), array('class' => 'form-control')) ?>
      </div>
    </div>


    <button name="submit" type="submit" class="btn btn-primary">Make Group</button>
  </div>
</form>
@stop