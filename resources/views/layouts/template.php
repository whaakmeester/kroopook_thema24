<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">
        <img height="20" alt="Kroopook" src="<?php echo Config::get('app.url'); ?>">
      </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <?php if(!(Auth::check())): ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <form class="navbar-form navbar-left" role="search" method="POST">
                    
            <?php echo Form::token(); ?>
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default">Sign In</button>
                </form>

        <li><a href="<?php echo URL::route('register'); ?>"><i class="fa fa-user"></i> Register</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  <?php else: ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="<?php echo URL::route('groups_edit'); ?>"><i class="fa fa-user"></i> Groep aanmaken</a></li>
        <li><a href="<?php echo URL::route('post_edit'); ?>"><i class="fa fa-user"></i> Post</a></li>
        <li><a href="<?php echo URL::route('logout'); ?>"><i class="fa fa-user"></i> Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  <?php endif; ?>
  </div><!-- /.container-fluid -->
</nav>


